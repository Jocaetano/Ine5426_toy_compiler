#include "node.h"

RootNode::RootNode(){}
RootNode::~RootNode(){}
void RootNode::insertNode(TreeNode* node){
	this->_rootNodes->push_back(node);
}

OperatorNode::OperatorNode(){}
OperatorNode::~OperatorNode(){}
OperatorNode* OperatorNode::set_left_child(TreeNode* node) {
	this->_left = node;
	return this;
}

BinaryOperatorNode::BinaryOperatorNode(const Operator& op) {
	this->_operator = op;
}
BinaryOperatorNode* BinaryOperatorNode::set_left_child(TreeNode* node) {
	this->_left = node;
	// this->check_type();
	return this;
}
BinaryOperatorNode* BinaryOperatorNode::set_right_child(TreeNode* node) {
	this->_right = node;
	return this;
}
BinaryOperatorNode* BinaryOperatorNode::set_children(TreeNode* node1, TreeNode* node2) {
	this->_left = node1;
	this->_right = node2;
	return this;
}

LiteralNode::LiteralNode(const InternalType& op, const char* value) {
	this->_type = "char";
    this->_iType = op;
	this->_s_value = value;
}
LiteralNode::LiteralNode(const InternalType& op, int value) {
	this->_type = "num";
	this->_iType = op;
    this->_i_value = value;
}
LiteralNode::LiteralNode(const InternalType& op, bool value) {
	this->_type = "bool";
	this->_iType = op;
    this->_b_value = value;
}
LiteralNode::LiteralNode(const InternalType& op, float value) {
	this->_type = "num";
	this->_iType = op;
    this->_f_value = value;
}
LiteralNode::~LiteralNode(){}