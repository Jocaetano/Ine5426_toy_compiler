#ifndef NODE_H
#define NODE_H

#include <iostream>
#include <vector>
#include <deque>
#include <llvm/IR/Value.h>

class CodeGenContext;
class NStatement;
class NExpression;
class NVariableDeclaration;

typedef std::vector<NStatement*> StatementList;
typedef std::vector<NExpression*> ExpressionList;
typedef std::vector<NVariableDeclaration*> VariableList;

class TreeNode
{
	public:
		TreeNode() {};
		virtual ~TreeNode() {};

		enum Operator {
			PLUS, MINUS, TIMES, DIVIDE, MOD, AND, OR, ATTRIBUTION, EQUAL, NOT_EQUAL, GREATER, LESS, GREATER_EQ, LESS_EQ
		};

		enum AccessOperator {
			STRUCT, ARRAY, CALL, ID
		};

		enum UnaryOperator {
			NOT, UN_PLUS, UN_MINUS
		};

		enum ReservedWord {
			RETURN, FOR, WHILE, IF, ELSE, BREAK
		};

		enum InternalType {
			INT, DOUBLE, BOOLEAN, STRING
		};

		// virtual std::string type() const;
		virtual llvm::Value* codeGen(CodeGenContext& context) { return NULL; }
};

class NExpression : public TreeNode {
};

class NStatement : public TreeNode {
};

class LiteralNode : public NExpression {
	public:
		LiteralNode(const InternalType& op, const char* value);
		LiteralNode(const InternalType& op, bool value);
		LiteralNode(const InternalType& op, int value);
		LiteralNode(const InternalType& op, float value);
		~LiteralNode();

		// std::string type() const {return std::string{_type};};
		virtual llvm::Value* codeGen(CodeGenContext& context);
	private:
		std::string _type;
		InternalType _iType;
		std::string _s_value;
		bool _b_value;
		int _i_value;
		float _f_value;
};

class NIdentifier : public NExpression {
public:
	std::string name;
	NIdentifier(const char* name) : name(name) { }
	virtual llvm::Value* codeGen(CodeGenContext& context);
};

class OperatorNode : public NExpression {
	public:
		OperatorNode();
		~OperatorNode();

		OperatorNode* set_left_child(TreeNode* node);
		// virtual std::string type() const;

		TreeNode* _left;
};

class BinaryOperatorNode : public OperatorNode {
	public:
		BinaryOperatorNode(const Operator& op);
		BinaryOperatorNode* set_children(TreeNode* node1, TreeNode* node2);
		BinaryOperatorNode* set_right_child(TreeNode* node);
		BinaryOperatorNode* set_left_child(TreeNode* node);

		virtual llvm::Value* codeGen(CodeGenContext& context);
	private:
		std::string _type;
		Operator _operator;
		TreeNode* _right;
};

class RootNode : public TreeNode {
	public:
		RootNode();
		~RootNode();
		void insertNode(TreeNode* node);
		virtual llvm::Value* codeGen(CodeGenContext& context);
	private:
		std::deque<TreeNode*>* _rootNodes;
};

class NAssignment : public NExpression {
public:
	NIdentifier& lhs;
	NExpression& rhs;
	NAssignment(NIdentifier& lhs, NExpression& rhs) : 
		lhs(lhs), rhs(rhs) { }
	virtual llvm::Value* codeGen(CodeGenContext& context);
};

class NBlock : public NExpression {
public:
	StatementList statements;
	NBlock() { }
	virtual llvm::Value* codeGen(CodeGenContext& context);
};

class NExpressionStatement : public NStatement {
public:
	NExpression& expression;
	NExpressionStatement(NExpression& expression) : 
		expression(expression) { }
	virtual llvm::Value* codeGen(CodeGenContext& context);
};

class NReturnStatement : public NStatement {
public:
	NExpression& expression;
	NReturnStatement(NExpression& expression) : 
		expression(expression) { }
	virtual llvm::Value* codeGen(CodeGenContext& context);
};

class NVariableDeclaration : public NStatement {
public:
	const NIdentifier& type;
	NIdentifier& id;
	NExpression *assignmentExpr;
	NVariableDeclaration(const NIdentifier& type, NIdentifier& id) :
		type(type), id(id) { assignmentExpr = NULL; }
	NVariableDeclaration(const NIdentifier& type, NIdentifier& id, NExpression *assignmentExpr) :
		type(type), id(id), assignmentExpr(assignmentExpr) { }
	virtual llvm::Value* codeGen(CodeGenContext& context);
};

#endif