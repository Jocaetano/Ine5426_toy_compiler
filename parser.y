%{
	#include "node.h"
	#include "codegen.h"
	#include <cstdio>
	#include <cstdlib>
	#include <iostream>
	#include <fstream>
	#include <sstream>
	NBlock *programBlock; /* the top level root node of our final AST */
	TreeNode* root;

	using namespace std;

	extern "C" int yylex();
	extern "C" int yyparse();
	extern "C" FILE *yyin;

	extern void yyerror(const char* s);

	void createCoreFunctions(CodeGenContext& context);
	ifstream open_file;
%}

/* Represents the many different ways we can access our data */
%union {
	TreeNode* node;
	RootNode* rootN;
	BinaryOperatorNode* biOpNode;
	NBlock *block;
	NExpression *expr;
	NStatement *stmt;
	NIdentifier *ident;
	NVariableDeclaration *var_decl;
	std::vector<NVariableDeclaration*> *varvec;
	std::vector<NExpression*> *exprvec;
	std::string *string;
	int token;
	TreeNode::Operator relOp;
	int ival;
	float fval;
	const char * charp;
	bool bval;
}

/* Define our terminal symbols (tokens). This should
   match our tokens.l lex file. We also define the node type
   they represent.
 */
%token <ival> INTLITERAL
%token <fval> FLOATLITERAL
%token <charp> ID
%token COMMA PERIOD SEMICOLON
%token OP_PARENS CL_PARENS OP_CURLY CL_CURLY
%token EQUAL NOT_EQUAL GREATER LESS GREATER_EQ LESS_EQ
%token ATTRIBUTION
%token PLUS MINUS TIMES DIVIDE

%token <token> TRETURN TEXTERN

/* Define the type of node our nonterminal symbols represent.
   The types refer to the %union declaration above. Ex: when
   we call an ident (defined by union type ident) we are really
   calling an (NIdentifier*). It makes the compiler happy.
 */
%type <ident> ident
%type <expr> numLiteral expr 
%type <block> program stmts
// %type <rootN> stmts
%type <biOpNode> numOp
%type <biOpNode> numOp1
%type <stmt> stmt
%type <relOp> comparison

/* Operator precedence for mathematical operators */
%left PLUS MINUS
%left TIMES DIVIDE

%start program

%%

program:
		stmts { programBlock = $1; }
		;
		
stmts:
		stmt { $$ = new NBlock(); $$->statements.push_back($<stmt>1); }
		| stmts stmt { $1->statements.push_back($<stmt>2); }
		;

stmt: 
		expr { $$ = new NExpressionStatement(*$1); }
		| TRETURN expr { $$ = new NReturnStatement(*$2); }
		;

ident: ID { $$ = new NIdentifier($1); }
	  ;

numLiteral:
		INTLITERAL {$$ = new LiteralNode(TreeNode::INT, $1);}
		| FLOATLITERAL {$$ = new LiteralNode(TreeNode::DOUBLE, $1);}
		;
	
expr:
		ident ATTRIBUTION expr { $$ = new NAssignment(*$<ident>1, *$3); }
		| ident { $<ident>$ = $1; }
		| numLiteral numOp {$$ = $2 != nullptr ? $2->set_left_child($1) : $1;}
		| expr numOp1 {$$ = $2->set_left_child($1); }
		| expr comparison expr { auto bOpNode = new BinaryOperatorNode($2); $$ = bOpNode->set_children($1, $3); }
		| OP_PARENS expr CL_PARENS { $$ = $2; }
		;

numOp:
		numOp1 {$$ = $1;}
		| {$$ = nullptr;}
		;

numOp1:
		PLUS expr {auto opNode = new BinaryOperatorNode(TreeNode::PLUS); $$ = opNode->set_right_child($2);}
		| MINUS expr {auto opNode = new BinaryOperatorNode(TreeNode::MINUS); $$ = opNode->set_right_child($2);}
		| TIMES expr {auto opNode = new BinaryOperatorNode(TreeNode::TIMES); $$ = opNode->set_right_child($2);}
		| DIVIDE expr {auto opNode = new BinaryOperatorNode(TreeNode::DIVIDE); $$ = opNode->set_right_child($2);}
		;

comparison:
		EQUAL { $$ = TreeNode::EQUAL; }
		| NOT_EQUAL { $$ = TreeNode::NOT_EQUAL; }
		| GREATER { $$ = TreeNode::GREATER; }
		| LESS { $$ = TreeNode::LESS; }
		| GREATER_EQ { $$ = TreeNode::GREATER_EQ; }
		| LESS_EQ { $$ = TreeNode::LESS_EQ; }
		;

%%

int main(int argc, char **argv) {
	#if YYDEBUG == 1
		yydebug = 1;
	#endif
	// open a file handle to a particular file:
	if (argc < 2){
		cout << "No file specified."<< endl;
		return 1;
	}
	FILE *myfile = fopen(argv[1], "r");
	// make sure it is valid:
	if (!myfile) {
		cout << "I can't open the file! :( " << endl;
		return -1;
	}
	open_file.open(argv[1], ios::in);
	// set flex to read from it instead of defaulting to STDIN:
	yyin = myfile;
	// parse through the input until there is no more:
	do {
		yyparse();
	} while (!feof(yyin));

	cout << programBlock << endl;
	// see http://comments.gmane.org/gmane.comp.compilers.llvm.devel/33877
	InitializeNativeTarget();
	InitializeNativeTargetAsmPrinter();
	InitializeNativeTargetAsmParser();
	CodeGenContext context;
	createCoreFunctions(context);
	context.generateCode(*programBlock);
	context.runCode();
	
	return 0;
}