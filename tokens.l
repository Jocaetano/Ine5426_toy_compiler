%{
#include <string>
#include "node.h"
#include "parser.hpp"

using namespace std;

#define YY_DECL extern "C" int yylex()

extern ifstream open_file;

void yyerror(const char *str) {
	cout << "<Line " << yylineno << "> "<< "Error: "<< str << endl;
	//exit(-1);
}

#define SAVE_TOKEN  yylval.string = new std::string(yytext, yyleng)
#define TOKEN(t)    (yylval.token = t)
%}
%option noyywrap
%option yylineno
%%

[ \t\n]					        ;
"//".*				            ;
"extern"                        return TOKEN(TEXTERN);
"return"				        return TOKEN(TRETURN);
[a-zA-Z_][a-zA-Z0-9_]*          {yylval.charp = strdup(yytext); return ID;}
[0-9]+\.[0-9]* 			        {yylval.fval = atof(yytext); return FLOATLITERAL;}
[0-9]+					        {yylval.ival = atoi(yytext); return INTLITERAL;}

">="							{return GREATER_EQ;}
"<="							{return LESS_EQ;}
"=="							{return EQUAL;}
"!="							{return NOT_EQUAL;}
"="								{return ATTRIBUTION;}
">"								{return GREATER;}
"<"								{return LESS;}

"("								{return OP_PARENS;}
")"								{return CL_PARENS;}
"{"								{return OP_CURLY;}
"}"								{return CL_CURLY;}

";"								{return SEMICOLON;}
","								{return COMMA;}
"."								{return PERIOD;}

"+"								{return PLUS;}
"-"								{return MINUS;}
"*"								{return TIMES;}
"/"								{return DIVIDE;}

%%
